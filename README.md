# `copier-python-meta`

Generate the `copier-python` template and then a project:

```shell
copier https://gitlab.com/copier-meta-templates-example/copier-python-meta copier-python
copier copier-python pyproj
```